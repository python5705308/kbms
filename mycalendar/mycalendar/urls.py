from django.contrib import admin
from django.urls import path, include
from calendarapp.views import home, CustomLoginView
from calendarapp import views as calendar_views  # Import your app's views
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from calendarapp.views import index  # Make sure to import the index view


urlpatterns = [
    path('', index, name='home'),

    path('admin/', admin.site.urls),
    path('calendar/', include('calendarapp.urls')),
    # path('', auth_views.LoginView.as_view(template_name='calendarapp/login.html'), name='login'),
    path('', CustomLoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),

    # path('', home, name='home'),  # Point the root URL to the home view
    path('login/', auth_views.LoginView.as_view(template_name='calendarapp/login.html'), name='login'),
    path('public-calendar/', calendar_views.public_calendar_view, name='public_calendar_home'),
    path('public-calendar/<int:year>/<int:month>/', calendar_views.public_calendar_view, name='public_calendar')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
