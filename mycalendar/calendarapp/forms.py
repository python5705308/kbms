from django import forms
from .models import DayPoints, Redeem, Kid, RedeemableItem, PointRequest
from datetime import date

class DayPointsForm(forms.ModelForm):
    kid_name = forms.ModelChoiceField(queryset=Kid.objects.all(), empty_label="Select Kid")

    class Meta:
        model = DayPoints
        fields = ['date', 'points', 'kid_name']  # Add 'kid_name' here
        widgets = {
            'date': forms.DateInput(attrs={'type': 'date'})
        }

class RedeemForm(forms.ModelForm):
    #kid_name = forms.ModelChoiceField(queryset=Kid.objects.all(), empty_label="Select Kid")
    redeemable_item = forms.ModelChoiceField(queryset=RedeemableItem.objects.all(), required=False)
    date_redeemed = forms.DateField(
        widget=forms.DateInput(format='%Y-%m-%d', attrs={'type': 'date'}),
        initial=date.today
    )
    class Meta:
        model = Redeem
        fields = ['kid_name', 'item_name', 'date_redeemed', 'points_redeemed', 'summary', 'redeemable_item']
        widgets = {
            'date_redeemed': forms.DateInput(attrs={'type': 'date'})
        }

    def __init__(self, *args, **kwargs):
        super(RedeemForm, self).__init__(*args, **kwargs)
        # self.fields['kid_name'].disabled = True
        # self.fields['date_redeemed'].disabled = True
        
class KidForm(forms.ModelForm):
    class Meta:
        model = Kid
        fields = ['name']

class PointRequestForm(forms.ModelForm):
    class Meta:
        model = PointRequest
        fields = ['requested_points', 'reason']