from django.db import models
from PIL import Image
from django.core.files.base import ContentFile
from io import BytesIO
from django.contrib.auth.models import User

class PointRequest(models.Model):
    kid_name = models.ForeignKey(User, on_delete=models.CASCADE)
    requested_points = models.IntegerField()
    reason = models.TextField()
    status = models.CharField(max_length=10, choices=[('pending', 'Pending'), ('approved', 'Approved'), ('rejected', 'Rejected')], default='pending')
    rejection_reason = models.TextField(blank=True, null=True)
    date_requested = models.DateField(auto_now_add=True)

    def __str__(self):
        return f"{self.kid_name.username} - {self.requested_points} points request"

class DayPoints(models.Model):
    date = models.DateField()
    points = models.IntegerField(default=0)
    kid_name = models.CharField(max_length=100) 
    redeemed_points = models.IntegerField(default=0)

    class Meta:
        unique_together = ('date', 'kid_name')  # Ensure uniqueness for the combination of date and kid_name

    def __str__(self):
        return f"{self.kid_name}: {self.date} - {self.points} points"

class Redeem(models.Model):
    kid_name = models.CharField(max_length=100)
    item_name = models.CharField(max_length=100)
    date_redeemed = models.DateField()
    points_redeemed = models.IntegerField()
    summary = models.TextField()

    class Meta:
        permissions = (
            ("can_redeem", "Can redeem points"),
        )

    def __str__(self):
        return f"{self.kid_name} redeemed {self.points_redeemed} points on {self.date_redeemed}"

class Kid(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name
    

class RedeemableItem(models.Model):
    name = models.CharField(max_length=100)
    points_required = models.IntegerField()
    image = models.ImageField(upload_to='redeemable_items/')

    def save(self, *args, **kwargs):
        if self.image:
            # Resize the image
            img = Image.open(self.image)
            img.thumbnail((80, 100), Image.Resampling.LANCZOS)  # Updated line

            # Save the resized image to a BytesIO object
            img_io = BytesIO()
            img.save(img_io, format='JPEG', quality=85)

            # Change the image field value to the new resized image
            self.image.save(self.image.name, ContentFile(img_io.getvalue()), save=False)

        super().save(*args, **kwargs)

    def __str__(self):
        return self.name