from django.shortcuts import render, redirect, get_object_or_404
from .models import DayPoints, Redeem, Kid, RedeemableItem, PointRequest
from .forms import DayPointsForm, RedeemForm, KidForm, RedeemableItem, PointRequestForm
from datetime import date
import calendar
from django.http import HttpResponse
from datetime import timedelta
from django.db.models import Sum, F
from django.core.exceptions import ValidationError
from django.contrib import messages
from django.shortcuts import redirect
from django.contrib.auth.views import LoginView
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required

def is_admin(user):
    return user.is_superuser

def home(request):
    return render(request, 'calendarapp/home.html')

class CustomLoginView(LoginView):
    template_name = 'calendarapp/login.html'

    def get_success_url(self):
        user = self.request.user
        if user.is_superuser:
            return '/calendar/'  # URL for admin
        else:
            return '/public-calendar/'  # URL for kids


@login_required
def calendar_view(request, year=date.today().year, month=date.today().month):
    if not request.user.is_superuser:
        messages.error(request, "You do not have permission to access this page.")
        return redirect('public_calendar_home')
    
    # month_days = calendar.monthrange(year, month)[1]
    first_day = date(year, month, 1)
# Calculate previous and next month
    first_day_of_current_month = date(year, month, 1)
    last_day_of_previous_month = first_day_of_current_month - timedelta(days=1)
    first_day_of_next_month = first_day_of_current_month + timedelta(days=calendar.monthrange(year, month)[1])

    if request.method == 'POST':
        form = DayPointsForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('calendar', year=year, month=month)
    else:
        form = DayPointsForm()

    points_list = list(DayPoints.objects.filter(date__year=year, date__month=month).values('id','date', 'points', 'kid_name'))
    total_points = DayPoints.objects.aggregate(Sum('points'))['points__sum'] or 0
    total_redeemed = DayPoints.objects.aggregate(Sum('redeemed_points'))['redeemed_points__sum'] or 0
    available_points = total_points - total_redeemed
    # Aggregate points and redeemed points per kid
    kids_points = DayPoints.objects.values('kid_name').annotate(
        total_points=Sum('points')
    ).order_by('kid_name')

    redeemed_points = Redeem.objects.values('kid_name').annotate(
        total_redeemed=Sum('points_redeemed')
    )

    # Convert redeemed_points to a dictionary for easier lookup
    redeemed_dict = {item['kid_name']: item['total_redeemed'] for item in redeemed_points}

    # Calculate available points
    for kid in kids_points:
        kid['total_redeemed'] = redeemed_dict.get(kid['kid_name'], 0)
        kid['available_points'] = kid['total_points'] - kid['total_redeemed']
    redeemed_points_list = Redeem.objects.filter(date_redeemed__year=year, date_redeemed__month=month).values('id', 'date_redeemed', 'item_name', 'points_redeemed', 'kid_name', 'summary')

    # Create a calendar that starts on Monday
    cal = calendar.Calendar(firstweekday=0)  # Monday is 0
    month_days = cal.monthdayscalendar(year, month)

    context = {
        'form': form,
        'year': year,
        'month': month,
        'points': {day.date.day: day.points for day in DayPoints.objects.filter(date__year=year, date__month=month)},
        'prev_month': last_day_of_previous_month.month,
        'prev_year': last_day_of_previous_month.year,
        'next_month': first_day_of_next_month.month,
        'next_year': first_day_of_next_month.year,
        'points_list': points_list,
        'total_points': total_points,
        'available_points': available_points,
        'kids_points': kids_points,
        'redeemed_points_list': redeemed_points_list,
        'is_admin': request.user.is_superuser,
        'month_days': month_days,
        }
    return render(request, 'calendarapp/calendar.html', context)

def public_calendar_view(request, year=date.today().year, month=date.today().month):
    # month_days = calendar.monthrange(year, month)[1]
    first_day = date(year, month, 1)
    # Calculate previous and next month
    first_day_of_current_month = date(year, month, 1)
    last_day_of_previous_month = first_day_of_current_month - timedelta(days=1)
    first_day_of_next_month = first_day_of_current_month + timedelta(days=calendar.monthrange(year, month)[1])

    points_list = list(DayPoints.objects.filter(date__year=year, date__month=month).values('id','date', 'points', 'kid_name'))
    total_points = DayPoints.objects.aggregate(Sum('points'))['points__sum'] or 0
    total_redeemed = DayPoints.objects.aggregate(Sum('redeemed_points'))['redeemed_points__sum'] or 0
    available_points = total_points - total_redeemed
    # Aggregate points and redeemed points per kid
    kids_points = DayPoints.objects.values('kid_name').annotate(
        total_points=Sum('points')
    ).order_by('kid_name')

    redeemed_points = Redeem.objects.values('kid_name').annotate(
        total_redeemed=Sum('points_redeemed')
    )

    # Convert redeemed_points to a dictionary for easier lookup
    redeemed_dict = {item['kid_name']: item['total_redeemed'] for item in redeemed_points}

    # Calculate available points
    for kid in kids_points:
        kid['total_redeemed'] = redeemed_dict.get(kid['kid_name'], 0)
        kid['available_points'] = kid['total_points'] - kid['total_redeemed']
    redeemed_points_list = Redeem.objects.filter(date_redeemed__year=year, date_redeemed__month=month).values('id', 'date_redeemed', 'item_name', 'points_redeemed', 'kid_name', 'summary')
    
    # No forms in context
  # Create a calendar that starts on Monday
    cal = calendar.Calendar(firstweekday=0)  # Monday is 0
    month_days = cal.monthdayscalendar(year, month)

    context = {
        'month_days': month_days,
        'year': year,
        'month': month,
        'points': {day.date.day: day.points for day in DayPoints.objects.filter(date__year=year, date__month=month)},
        'prev_month': last_day_of_previous_month.month,
        'prev_year': last_day_of_previous_month.year,
        'next_month': first_day_of_next_month.month,
        'next_year': first_day_of_next_month.year,
        'points_list': points_list,
        'total_points': total_points,
        'available_points': available_points,
        'kids_points': kids_points,
        'redeemed_points_list': redeemed_points_list,
        }
    return render(request, 'calendarapp/public_calendar.html', context)


@login_required
@user_passes_test(is_admin)
def delete_day_points(request, day_points_id):
    day_points = get_object_or_404(DayPoints, pk=day_points_id)
    if request.method == "POST":
        day_points.delete()
        return redirect('calendar')  # Redirect back to the calendar view
    return render(request, 'calendarapp/delete_confirm.html', {'day_points': day_points})

@login_required
@permission_required('calendarapp.can_redeem', raise_exception=True)
def redeem_points(request):
    today = date.today()
    redeemable_items = RedeemableItem.objects.all()

    try:
        kid = Kid.objects.get(name=request.user)
    except Kid.DoesNotExist:
        kid = None    

    total_points = DayPoints.objects.filter(kid_name=kid).aggregate(Sum('points'))['points__sum'] or 0
    redeemed_points = Redeem.objects.filter(kid_name=kid).aggregate(Sum('points_redeemed'))['points_redeemed__sum'] or 0
    available_points = total_points - redeemed_points

    initial_data = {
        'date_redeemed': date.today(),  # Default to today's date
        'kid_name': kid,  # Default to the logged-in user's username
    }

    if request.method == 'POST':
        form = RedeemForm(request.POST)
        if form.is_valid():
            redeem = form.save(commit=False)
            # Ensure points do not exceed available points
            total_points = DayPoints.objects.filter(kid_name=redeem.kid_name).aggregate(Sum('points'))['points__sum'] or 0
            redeemed_points = Redeem.objects.filter(kid_name=redeem.kid_name).aggregate(Sum('points_redeemed'))['points_redeemed__sum'] or 0
            available_points = total_points - redeemed_points

            if redeem.points_redeemed > available_points:
                raise ValidationError('Redeemed points cannot exceed available points.')
            redeem.save()
            return redirect('calendar')  # Redirect back to the calendar view
        else:
            print(form.errors)
            # If form is not valid, render the form with errors
            return render(request, 'calendarapp/redeem_form.html', {'form': form})
    else:
        form = RedeemForm(initial=initial_data)

    available_points = total_points - redeemed_points
    context = {
        'form': form,
        'today': today,
        'total_points': total_points,
        'redeemable_items': redeemable_items,
        'available_points': available_points,
    }
    return render(request, 'calendarapp/redeem_form.html', context)

@login_required
@user_passes_test(is_admin)
def delete_redeem(request, redeem_id):
    redeem_entry = get_object_or_404(Redeem, pk=redeem_id)
    if request.method == "POST":
        redeem_entry.delete()
        return redirect('calendar')  # Redirect back to the calendar or an appropriate page
    return render(request, 'calendarapp/delete_redeem_confirm.html', {'redeem_entry': redeem_entry})

@login_required
@user_passes_test(is_admin)
def remove_redemption(request, redemption_id):
    redemption = get_object_or_404(Redeem, pk=redemption_id)
    if request.method == "POST":
        redemption.delete()
        return redirect('calendar')  # Redirect back to the calendar view
    return render(request, 'calendarapp/remove_redemption_confirm.html', {'redemption': redemption})

def add_kid(request):
    if request.method == 'POST':
        form = KidForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('some_view')
    else:
        form = KidForm()
    return render(request, 'calendarapp/add_kid.html', {'form': form})

# Update Kid
def update_kid(request, kid_id):
    kid = get_object_or_404(Kid, pk=kid_id)
    if request.method == 'POST':
        form = KidForm(request.POST, instance=kid)
        if form.is_valid():
            form.save()
            return redirect('some_view')
    else:
        form = KidForm(instance=kid)
    return render(request, 'calendarapp/update_kid.html', {'form': form, 'kid': kid})

# Delete Kid
def delete_kid(request, kid_id):
    kid = get_object_or_404(Kid, pk=kid_id)
    if request.method == 'POST':
        kid.delete()
        return redirect('some_view')
    return render(request, 'calendarapp/delete_kid_confirm.html', {'kid': kid})

def index(request):
    return render(request, 'calendarapp/index.html')

@login_required
def request_points(request):
    if request.method == 'POST':
        form = PointRequestForm(request.POST)
        if form.is_valid():
            point_request = form.save(commit=False)
            point_request.kid_name = request.user
            point_request.save()
            return redirect('view_requests')  # Redirect to a page where kids can see their request status
    else:
        form = PointRequestForm()

    return render(request, 'calendarapp/request_points.html', {'form': form})

@login_required
def view_requests(request):
    # Fetch the logged-in kid's requests
    point_requests = PointRequest.objects.filter(kid_name=request.user).order_by('-date_requested')

    return render(request, 'calendarapp/view_requests.html', {'point_requests': point_requests})

@login_required
@user_passes_test(is_admin)
def manage_point_requests(request):
    pending_requests = PointRequest.objects.filter(status='pending')

    if request.method == 'POST':
        request_id = request.POST.get('request_id')
        action = request.POST.get('action')
        rejection_reason = request.POST.get('rejection_reason', '')

        point_request = get_object_or_404(PointRequest, id=request_id)

        if action == 'approve':
            # Approve the request and add points to DayPoints
            day_points, created = DayPoints.objects.get_or_create(date=date.today(), kid_name=point_request.kid_name.username)
            day_points.points += point_request.requested_points
            day_points.save()
            point_request.status = 'approved'
        elif action == 'reject':
            point_request.status = 'rejected'
            point_request.rejection_reason = rejection_reason
        point_request.save()

        return redirect('manage_point_requests')  # Refresh the page

    return render(request, 'calendarapp/manage_point_requests.html', {'pending_requests': pending_requests})