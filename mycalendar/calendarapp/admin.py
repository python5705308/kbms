from django.contrib import admin
from .models import *

admin.site.register(Kid)
admin.site.register(DayPoints)
admin.site.register(Redeem)
admin.site.register(RedeemableItem)