from django.urls import path
from . import views

urlpatterns = [
    path('<int:year>/<int:month>/', views.calendar_view, name='calendar'),
    path('', views.calendar_view, name='calendar'),  # Ensure this name is 'calendar'
    path('delete/<int:day_points_id>/', views.delete_day_points, name='delete_day_points'),
    #path('redeem/', views.redeem_points, name='redeem'),
    path('redeem/', views.redeem_points, name='redeem_points'),
    path('delete_redeem/<int:redeem_id>/', views.delete_redeem, name='delete_redeem'),
    path('remove-redemption/<int:redemption_id>/', views.remove_redemption, name='remove_redemption'),
    path('request-points/', views.request_points, name='request_points'),
    path('manage-point-requests/', views.manage_point_requests, name='manage_point_requests'),
    path('view-requests/', views.view_requests, name='view_requests'),

]
